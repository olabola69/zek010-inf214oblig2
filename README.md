##Task 1

    Notes for task 1
    Task one contains Java concurrency, by utilising the properties of threads(wait, clear).
    SPEED: 

##Task 2

    Notes for task 2:
    2.1 public String[] searchQuery returns a query to string, and splits the query 
    on the key ";". 
    
    2.2 The IndexingTask class will parse the document to get its vocabulary, by implementing the "Callable" interface 
    with Document as a parameter. The File object is the document being parsed. The call() method parses the document
    to find the vocabulary of the document, and returns the data found, together with the document object.
    
    2.3 If the thread is no interrupted it makes a document into a completionService with a 
    take-method to create an array which cant be made bigger or smaller than it is on creation.
    The run() method obtains the future object from CompletionService.take().get(). The while(true)
    will run until the thread is interrupted, and when it is interrupted, it uses the poll() method
    to process all the Future objects. The inverted index is updated with the object from the take()
    method by using the updateInvertedIndex() method. The updateInvertedIndex() method uses the parameters
    vocabulary from document, inverted index and the file name, processing all the words.
    If a word is not in the invertedIndex, the updateInvertedIndex() method will use
    computeIfAbsent() method to add it.
    
    2.4
    ConcurrentIndexing initilizes its variables ConcurrentHashMap for storing the inverted index,
    ExecutorComletionService for executing the tasks, and executionTime which returns
    the runtime of the processes with the getExecutionTime()-method.
    It creates two threads to execute two InvertedIndexTask.
    For every file it creates an InvertedTask object and delivers it to the CompletionService
    class with the submit()-method. There is a prevetion where if the queue of tasks is larger 
    than size 1000, the thread sleeps. At the end it shuts down with the shutdown()-method
    and a awaitTermination()-method which returns when the InvertedTask has finished.
    It then interrupts the remaining threads.
    
    
    Source: "Mastering Concurrency Programming with Java 9" (page 156-160) - by Javier Fernandez Gonzalez
    
    
    
##Task 3
    
    3.1
    n is the number of processes, and m is the number of threads, find the answer using
    this formula: (n*m)! / m!^n
    (3*2)! / 2!^3 = 720 / 8 = 90
    
    3.2
    printer() {'
        semaphore s1 = new semaphore(0);
        semaphore s2 = new semaphore(0);
        
        process P1 {
        write("1"); 
        write("2"); 
        s1.V();
        
        }
        process P2 {
        s1.P();
        write("3"); 
        write("4"); 
        s2.V();
        }
        process P3 {
        s2.P()
        write("5");
        write("6");
        }
        
##Task 4

        4.2
        printer() {'
                Sem s1 = new Sem(0);
                Sem s2 = new Sem(0);
                
                process P1 {
                write("1"); 
                write("2"); 
                s1.release();
                
                }
                process P2 {
                s1.aquire();
                write("3"); 
                write("4"); 
                s2.release();
                }
                process P3 {
                s2.aquire();
                write("5");
                write("6");
                }
                
                
##Task 5

    Imports:
    CyclicBarrier is a syncrhonization tool for allowing a set of threads to wait for each other
    to reach a barrier. The cyclic portion is that this can be done again after the waiting
    threads have been released.
    BrokenBarrierException throws an exception when a thread tries to wait on another thread that is
    in a broken state.