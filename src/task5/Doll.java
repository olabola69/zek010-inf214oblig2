package src.task5;

public class Doll {
    int id;
    int qualityScoreMachine;
    boolean imperfect, isPainted;

    public Doll(int id, int i) {
        return;
    }

    @Override
    public String toString() {
        return "Doll{" +
                "id=" + id +
                ", qualityScoreMachine=" + qualityScoreMachine +
                ", imperfect=" + imperfect +
                ", isPainted=" + isPainted +
                '}';
    }

    public boolean setPainted(boolean b) {
        return isPainted;
    }

    public int getQualityScore() {
        return qualityScoreMachine;
    }

    public boolean hasImperfections(boolean b) {
        return imperfect;
    }
}
