package src.task5;

import org.w3c.dom.ls.LSOutput;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class DollFactory {
    List<Doll> dolls;
    public CyclicBarrier stageA, stageB, stageC;
    public void execution(int dollsNumber) throws InterruptedException {
        stageA = new CyclicBarrier(dollsNumber);
        stageB = new CyclicBarrier(dollsNumber);
        // TODO stageC = new CyclicBarrier(...);
        stageC = new CyclicBarrier(dollsNumber + 1);
        dolls = new ArrayList<>(dollsNumber);

        for (int i = 0; i < dollsNumber; i++) {
            Process task = new Process(i, dolls, stageA, stageB, stageC);
            // TODO: Your solution goes here
            Thread thread = new Thread(task);
            thread.start();
            }
        try {
            stageC.await();
            System.out.println("Packaging process D");
            // TODO: print results
            System.out.println(stageA.toString());
            System.out.println(stageB.toString());
            System.out.println(stageC.toString());
            for (Doll d : dolls) {
                System.out.println(d.toString());
            }

            } catch (BrokenBarrierException e) {
            e.printStackTrace(); }}// c
}
