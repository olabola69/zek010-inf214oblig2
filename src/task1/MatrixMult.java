package src.task1;

import java.util.ArrayList;
import java.util.List;

public class MatrixMult {
    public static void multiply( double[][] A, double[][] B, double[][] C) {
        List<Thread> threads = new ArrayList<>();
        for (int i = 0; i < A.length; i++) {
            RowMultiplierTask task = new RowMultiplierTask(C, A, B, i);
            Thread thread = new Thread(task);
            // TODO: Your solution goes here
            thread.start();    //hmm
            threads.add(thread);   //hmm
            if (threads.size() % 10 == 0) waitForThreads(threads);}
        // TODO: Your solution goes here
            waitForThreads(threads);
        }

        private static void waitForThreads(List<Thread> threads) {
        for (Thread thread : threads) {
            try {
                // TODO: Your solution goes here
                thread.join();
                } catch (InterruptedException e) {
                e.printStackTrace();}}
        threads.clear(); }
}
